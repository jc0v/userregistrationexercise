# README #

This application consists of a REST web service written using Spring Boot that can register a user that isn't blacklisted and has credentials matching the following criteria.

username (alphanumerical, no spaces)  
password (at least four characters, at least one upper case character, at least one number)  
date of birth (ISO 8601)  

To run this application ensure you have Java installed and then run "gradlew bootRun" from the command line.  
To run the unit tests run "gradlew test"  

API Doc  
Title : Register a user  
URL : /register  
Method : POST  
Request Body : {"username": "username", "password": "Password01", "dob": "1985-10-18", "ssn": "123456789"}  
Response Codes: Created (201), Bad Request (400), Conflict (409)   

Title : Get all users  
URL : /user  
Method : GET  
Response Codes: OK (200), No Content (204)  

Title : Get a user  
URL : /user/{username}  
Method : GET  
Path Param : username  
Response Codes: OK (200), Not Found (404)  