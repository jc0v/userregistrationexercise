package com.gamesys;

import static org.junit.Assert.*;

import org.apache.commons.lang3.StringUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import com.gamesys.beans.User;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment=WebEnvironment.RANDOM_PORT)
public class RegistrationControllerIntegrationTest {

	@Autowired
	private TestRestTemplate restTemplate;
	
	@Test
	public void testRegisterAndReturnUser() {
		ResponseEntity<String> response = restTemplate.postForEntity("/register", new User("TestIntUser", "Password01", "1980-02-02", "123456789"), String.class);
		assertTrue(HttpStatus.CREATED.equals(response.getStatusCode()));
		
		ResponseEntity<User> userResponse = restTemplate.getForEntity("/user/TestIntUser", User.class);
		assertTrue(HttpStatus.OK.equals(userResponse.getStatusCode()));
		assertTrue(StringUtils.equalsIgnoreCase(userResponse.getBody().getDob(), "1980-02-02"));
	}

}
