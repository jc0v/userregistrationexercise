package com.gamesys;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import org.apache.commons.lang3.StringUtils;
import org.junit.Test;
import org.mockito.ArgumentMatchers;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import com.gamesys.beans.User;
import com.gamesys.dao.impl.RegistrationDAOImpl;
import com.gamesys.rest.RegistrationController;
import com.gamesys.service.ExclusionService;
import com.gamesys.service.impl.ExclusionServiceImpl;

public class RegistrationControllerTest {

	@Test
	public void testRegisterAndRetrieveUser() {
		RegistrationController rc = new RegistrationController();
		rc.setExclusionService(new ExclusionServiceImpl());
		rc.setRegistrationDAO(new RegistrationDAOImpl());
		ResponseEntity<String> response = rc.registerUser(new User("TestUser", "Password01", "1980-01-01", "123456789"));
		assertTrue(HttpStatus.CREATED.equals(response.getStatusCode()));
		
		ResponseEntity<User> response2 = rc.getUser("TestUser");
		assertTrue(StringUtils.equalsIgnoreCase(response2.getBody().getUsername(), "TestUser"));
		assertTrue(StringUtils.equalsIgnoreCase(response2.getBody().getSsn(), "123456789"));
	}

	@Test
	public void testInvalidPassword() {
		RegistrationController rc = new RegistrationController();
		ResponseEntity<String> response = rc.registerUser(new User("TestUser", "password01", "1980-01-01", "123456789"));
		assertTrue(HttpStatus.BAD_REQUEST.equals(response.getStatusCode()));
		response = rc.registerUser(new User("TestUser", "Password", "1980-01-01", "123456789"));
		assertTrue(HttpStatus.BAD_REQUEST.equals(response.getStatusCode()));
		response = rc.registerUser(new User("TestUser", "Pa5", "1980-01-01", "123456789"));
		assertTrue(HttpStatus.BAD_REQUEST.equals(response.getStatusCode()));
	}
	
	@Test
	public void testDuplicateRegistration() {
		RegistrationController rc = new RegistrationController();
		rc.setExclusionService(new ExclusionServiceImpl());
		rc.setRegistrationDAO(new RegistrationDAOImpl());
		ResponseEntity<String> response = rc.registerUser(new User("TestUser2", "Password02", "1980-01-01", "123456789"));
		assertTrue(HttpStatus.CREATED.equals(response.getStatusCode()));
		
		response = rc.registerUser(new User("TestUser2", "Password02", "1980-01-01", "123456789"));
		assertTrue(HttpStatus.CONFLICT.equals(response.getStatusCode()));
	}
	
	@Test
	public void testBlockedUser() {
		RegistrationController rc = new RegistrationController();
		rc.setRegistrationDAO(new RegistrationDAOImpl());
		
		// Mock out ExclusionService to block user creation
		ExclusionService es = mock(ExclusionService.class);
		when(es.validate(ArgumentMatchers.anyString(), ArgumentMatchers.anyString())).thenReturn(false);
		rc.setExclusionService(es);
		
		ResponseEntity<String> response = rc.registerUser(new User("TestUser3", "Password03", "1980-01-01", "123456789"));
		assertTrue(HttpStatus.BAD_REQUEST.equals(response.getStatusCode()));
		assertTrue(StringUtils.contains(response.getBody(), "GameSys"));
	}
}
