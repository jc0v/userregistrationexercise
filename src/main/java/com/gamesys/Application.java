package com.gamesys;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Main Spring Boot Application class
 * @author joshu
 *
 */
@SpringBootApplication
public class Application {
	public static void main (String[] args) {
		SpringApplication.run(Application.class, args);
	}
}
