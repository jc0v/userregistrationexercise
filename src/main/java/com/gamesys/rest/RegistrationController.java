package com.gamesys.rest;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.gamesys.beans.User;
import com.gamesys.dao.RegistrationDAO;
import com.gamesys.service.ExclusionService;

/**
 * Spring Rest Controller class to handle user registration and retrieval http requests
 * @author joshu
 *
 */
@RestController
public class RegistrationController {
	
	@Autowired
	private ExclusionService exclusionService;

	@Autowired
	private RegistrationDAO registrationDAO;
	
	/**
	 * Registers a user performing the following checks:
	 * parameter's are valid according to specification below
	 * user hasn't been already registered
	 * user is not blacklisted (see ExclusionService)
	 * 
	 * username - alphanumeric, no spaces
	 * password - at least four characters, at least one upper case character, at least one number
	 * dob - ISO 8601 format
	 * ssn - not validated, for better understanding see: http://en.wikipedia.org/wiki/Social_Security_number
	 */	
	@RequestMapping(path="register", method=RequestMethod.POST)
	public ResponseEntity<String> registerUser(@RequestBody User user) {
		// user name (alphanumeric, no spaces)
		if (!StringUtils.isAlphanumeric(user.getUsername())) {
			return new ResponseEntity<String>("User Name must be alphanumeric and contain no spaces", HttpStatus.BAD_REQUEST);
		}
		if (StringUtils.length(user.getPassword()) < 4 ||
				!StringUtils.isMixedCase(user.getPassword()) ||
				!StringUtils.containsAny(user.getPassword(), "0", "1", "2", "3", "4", "5", "6", "7", "8", "9")) {
			return new ResponseEntity<String>("Password must be at least four characters and contain at least one upper case character and at least one number", HttpStatus.BAD_REQUEST);
		}
		try {
			new SimpleDateFormat("yyyy-MM-dd").parse(user.getDob());
		} catch (ParseException e) {
			return new ResponseEntity<String>("Date Of Birth must be in ISO 8601 format. eg 1985-10-18", HttpStatus.BAD_REQUEST);
		}
		
		// Check if user has been blacklisted
		if (!exclusionService.validate(user.getDob(), user.getSsn())) {
			return new ResponseEntity<String>("Unable to register this user. Please contact GameSys for more information.", HttpStatus.BAD_REQUEST);
		}
		
		if (registrationDAO.getUser(user.getUsername()) != null) {
			return new ResponseEntity<String>("This user already exists and cannot be reregistered.", HttpStatus.CONFLICT);
		}
		
		// Persist User
		registrationDAO.persistUser(user.getUsername(), user.getPassword(), user.getDob(), user.getSsn());
		return new ResponseEntity<String>("User successfully created", HttpStatus.CREATED);		
	}
	
	@RequestMapping(path="user", method=RequestMethod.GET)
	public ResponseEntity<List<User>> getAllUsers() {
		List<User> users = registrationDAO.getAllUsers();
		if (users.isEmpty()) {
			return new ResponseEntity<List<User>>(HttpStatus.NO_CONTENT);
		}
		return new ResponseEntity<List<User>>(users, HttpStatus.OK);
	}
	
	@RequestMapping(path="user/{username}", method=RequestMethod.GET)
	public ResponseEntity<User> getUser(@PathVariable String username) {
		User user = registrationDAO.getUser(username);
		if (user == null) {
			return new ResponseEntity<User>(HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<User>(user, HttpStatus.OK);
	}
	
	// Setters to allow easy mocking
	public void setExclusionService(ExclusionService exclusionService) {
		this.exclusionService = exclusionService;
	}

	public void setRegistrationDAO(RegistrationDAO registrationDAO) {
		this.registrationDAO = registrationDAO;
	}
}