package com.gamesys.beans;

/**
 * Bean that represents a user account
 * @author joshu
 *
 */
public class User {

	private String username;
	private String password;
	private String dob;
	private String ssn;

	public User () {}
	
	public User (String username, String password, String dob, String ssn) {
		this.username = username;
		this.password = password;
		this.dob = dob;
		this.ssn = ssn;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getDob() {
		return dob;
	}

	public void setDob(String dob) {
		this.dob = dob;
	}

	public String getSsn() {
		return ssn;
	}

	public void setSsn(String ssn) {
		this.ssn = ssn;
	}
	
}
