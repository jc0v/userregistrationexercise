package com.gamesys.dao.impl;

/**
 * Was used when using dob and ssn as user identifier like the exclusion service
 * but decided against it.
 * @author joshu
 *
 */
@Deprecated
public class UserKey {
	private String dob;
	private String ssn;
	
	public UserKey(String dob, String ssn) {
		this.dob = dob;
		this.ssn = ssn;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((dob == null) ? 0 : dob.hashCode());
		result = prime * result + ((ssn == null) ? 0 : ssn.hashCode());
		return result;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		UserKey other = (UserKey) obj;
		if (dob == null) {
			if (other.dob != null)
				return false;
		} else if (!dob.equals(other.dob))
			return false;
		if (ssn == null) {
			if (other.ssn != null)
				return false;
		} else if (!ssn.equals(other.ssn))
			return false;
		return true;
	}
	
}
