package com.gamesys.dao.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.stereotype.Component;

import com.gamesys.beans.User;
import com.gamesys.dao.RegistrationDAO;

/**
 * Dummy impl of RegistrationDAO using a static map as a datastore
 * @author joshu
 *
 */
@Component
public class RegistrationDAOImpl implements RegistrationDAO{
	private static Map<String, User> data = new HashMap<>();
	
	@Override
	public User getUser(String username) {
		return data.get(username);
	}
	
	@Override
	public void persistUser(String username, String password, String dob, String ssn) {
		data.put(username, new User(username, password, dob, ssn));
	}

	@Override
	public List<User> getAllUsers() {
		return data.values().stream().collect(Collectors.toList());
	}
}
