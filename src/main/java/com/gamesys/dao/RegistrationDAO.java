package com.gamesys.dao;

import java.util.List;

import com.gamesys.beans.User;

/**
 * RegistrationDAO Interface. Could be implemented using a persistence framework.
 * @author joshu
 *
 */
public interface RegistrationDAO {
	
	public User getUser(String username);
	public List<User> getAllUsers();
	public void persistUser(String username, String password, String dob, String ssn);
}
